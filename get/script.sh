#!/bin/bash
#version: 1.1

version="1.1"
time_zone="Europe/Lisbon"

repo_name="asterisk.repo"  # repo

file="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")" # files
folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
tmp_folder="$(dirname $(mktemp -u))"
sys_folder="/etc/system.d"
lockfile="/var/lock/`basename $0`"
lockfd=99

label_start="ZNS1" # goto
goto_label="$sys_folder/install/tmp/goto"

log=1 # log
log_file="$sys_folder/install/log/asterisk.log"
yum_file="$sys_folder/install/log/yum.log"
log_error="$sys_folder/install/log/error.log"


# _ functions 
c_stored(){
	
	if [ ! -f $goto_label ]; then 
		i=1
		mkdir -p "$(dirname "$goto_label")"
		echo "$i ZNS$i $folder/$file" > $goto_label		
	else
		NC=$(cat $goto_label)
		set -- "$NC"
		IFS=" "; declare -a Array=($*) 
		i="${Array[0]}"
		label_start="${Array[1]}"
	fi

}

s_goto(){
    label=$1
    cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
    eval "$cmd"
    exit
}

n_reboot(){
	e_log "reboot: ZNS$1"
	echo "$i ZNS$1 $folder/$file" > $goto_label	
	r_cmd "$shutdown -r now"
	exit
}

e_log(){
	if [[ $log == 1 ]]; then 
		if [ ! -f $log_file ]; then mkdir -p "$(dirname "$log_file")"; touch $log_file; fi # check if log_file exists
		if [[ "$2" == "" ]]; then i=$[i + 1]; fi # keep counter if not blank
		if [[ "$i" -gt "2" ]]; then dt=$(date '+%d/%m/%Y %H:%M:%S,%3N'); else dt=$(date '+%d/%m/%Y %H:%M:%S,%3N +%Z'); fi # show timezone i>2
		
		echo -e "[ $dt ] ($i)" $1 >> $log_file
	fi
	
	echo -e ""
	if [[ "$2" != "off" ]]; then # display only selected logs
		echo -e "( $i ) $1"
	else
		echo -e "( $i ) Loading..."
	fi 
}

r_cmd(){
	
	exe=$1
	tmp_file=$(mktemp $tmp_folder/$file.XXXXXXXXXX)
	
	# execute command	
	if [[ "$exe" =~ ^$curl.* ]]; then	
		IFS=" "; declare -a Array=($*) 
		url=`echo "${Array[1]//\'}"`
		saved_file=`echo "${Array[2]//\'}"`
		
		NC=$($curl -s -w "%{size_download} %{http_code} %{speed_download} %{time_total}" -o $saved_file $url)
		set -- "$NC" 
		IFS=" "; declare -a Array=($*) 
		size_download="${Array[0]}"
		http_code="${Array[1]}"
		speed_download="${Array[2]}"
		time_total="${Array[3]}"

		if [ $size_download -eq 0 ] || [ $http_code -ne '200' ]; then STATUS=1; else STATUS=0; fi
	
		echo 'url: '$url > $tmp_file		
		printf 'size downloaded: ' >> $tmp_file; printf $size_download | awk '{ foo = $1 / 1024 ; print foo " Kb" }' >> $tmp_file
		echo 'http_code: '$http_code >> $tmp_file
		printf 'download speed: ' >> $tmp_file; printf $speed_download | awk '{ foo = $1 / 1024 ; print foo " Kb/s" }' >> $tmp_file		
		echo 'total time: '$time_total' s' >> $tmp_file
		echo 'file destination: '$saved_file >> $tmp_file
		
		exe="$curl -s -o $saved_file --compressed $url"
		printf "# $exe \n"
		cat $tmp_file
		
		
	else
		printf "# $exe "
		
		NC="$(`eval $exe > $tmp_file`)" 
		STATUS=$?
		
		if [[ "$2" != "" ]] && [ $2 -eq 0 ]; then STATUS=1; fi # log error from r_cmd 
	fi
	
	
	# report
	dt=$(date '+%H:%M:%S,%3N')
		
	cat $tmp_file | sed -r 's/\x1B\(B\x1B\[m//g' | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g' > $tmp_file.tmp
		
	if [[ $log == 1 ]] && [ $STATUS -eq 0 ]; then 	
		echo -e "$dt #" $exe >> $log_file	
		
		# write in yum.log
		if [[ "$exe" =~ ^$yum.* ]]; then
			echo -e "$dt #" $exe >> $yum_file	
			$head --lines=-0 $tmp_file.tmp >> $yum_file	
		else
			$head --lines=-0 $tmp_file.tmp >> $log_file		
		fi
		
	fi	
	
	red=$(tput setaf 1); green=$(tput setaf 2); normal=$(tput sgr0)
	
	if [ $STATUS -eq 0 ] ; then 
		printf "[ "$green" OK "$normal" ]\n"
				
		rm -f $tmp_file $tmp_file.tmp
	else 
		touch $log_error
		echo -e "[ $dt ] #" $exe > $log_error			
		$head --lines=-0 $tmp_file.tmp >> $log_error
		
		printf "[ "$red" Fail "$normal" ]\n"			
		
		rm -f $tmp_file $tmp_file.tmp
		exit 1
	fi 
	
}


# _ commands
commands=( "yum" "service" "chkconfig" "sestatus" "setenforce" "sysctl" "shutdown" "adduser" "curl" "head" )
for command in "${commands[@]}"
do
	eval "${command//-/_}=$(command -v $command)"
done


# _ lock file
_lock()             { flock -$1 $lockfd; }
_no_more_locking()  { _lock u; _lock xn && rm -f $lockfile; }
_prepare_locking()  { eval "exec $lockfd>\"$lockfile\""; trap _no_more_locking EXIT; }

_prepare_locking

exlock_now()        { _lock xn; }  # obtain an exclusive lock immediately or fail
exlock()            { _lock x; }   # obtain an exclusive lock
shlock()            { _lock s; }   # obtain a shared lock
unlock()            { _lock u; }   # drop a lock

exlock_now || exit 1


# _ stored vars
c_stored



# _ start routine
start=${1:-"$label_start"}
e_log "goto: $start" off
s_goto $start



ZNS1:
# time
e_log "setup ntp time: $(date)"
r_cmd "$yum -y install ntp ntpdate"
r_cmd "$service ntpd restart"
r_cmd "$chkconfig ntpd on"
r_cmd "ln -sf /usr/share/zoneinfo/$time_zone /etc/localtime"
e_log "ntp time checkup: $(date)" 0

# bash_install profile.d
e_log "download file_install: /etc/profile.d/bash.sh"
r_cmd "$yum -y install bind-utils"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/7e1416d5490aaabaced08cd6f082f91e79b7fbe9/get/centos/6/x86_64/bash_install.sh' '/etc/profile.d/bash.sh'"

# SElinux
e_log "disable: SElinux"
if $sestatus | grep -q 'enabled' ; then
	sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/sysconfig/selinux
	sed -i 's/\(^SELINUX=\).*/\SELINUX=disabled/' /etc/selinux/config
	r_cmd "$setenforce 0"
else
	r_cmd "echo SElinux already disabled"
fi

# remove services
e_log "remove services"
services=( "postfix" "ip6tables" "mdmonitor" "netfs" )
for srv in "${services[@]}"
do
	NC=$(ps aux | grep -v grep | grep -c $srv)
	if [ $? == 0 ]; then r_cmd "$service $srv stop"; fi
	r_cmd "$chkconfig --del $srv"
done

# sysctl
e_log "tweak sysctl"
csysctl=
csysctl+=$'\n'
csysctl+=$'net.ipv6.conf.default.disable_ipv6 = 1\n'
csysctl+=$'net.ipv6.conf.all.disable_ipv6 = 1\n'
csysctl+=$'\n'
csysctl+=$'# This makes the machine only respond to arps when the arp is received on the correct interface\n'
csysctl+=$'net.ipv4.conf.all.arp_ignore = 1\n'
csysctl+=$'# Reboot machine automatically after 20 seconds if it kernel panics\n'
csysctl+=$'kernel.panic = 20\n'
if [[ `cat /etc/sysctl.conf` != *`echo $csysctl`* ]]; then
	echo $csysctl >> /etc/sysctl.conf
	r_cmd "$sysctl -p"
else
	r_cmd "echo sysctl already tweaked"
fi

# reboot
n_reboot 2



ZNS2:
# selinux test
e_log "check: SElinux"
if echo `$sestatus` | grep -q 'enabled' ; then
	r_cmd "echo Not possible to disable SELinux" 0
fi

# speed tool
e_log "speed tools: /etc/system.d/speed.sh"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/93f9f3e2c66d81e8d194449d936a324e8929635e/get/centos/6/x86_64/tools/speed.sh' '/etc/system.d/speed.sh'"

# shmz repos
e_log "shmz repos: /etc/yum.repos.d/$repo_name"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/e90d1ef19e6e350593c972a8fb5659397e4a6f22/get/centos/6/x86_64/asterisk.repo' '/etc/yum.repos.d/$repo_name'"

# packages; updates
e_log "clean&update packages"
r_cmd "$yum clean all"
r_cmd "$yum -y update"

# asterisk user
e_log "add asterisk user"
user_exists=$(id -u asterisk > /dev/null 2>&1; echo $?)
if [ $user_exists -eq 1 ] ; then	
	r_cmd "$adduser asterisk -M -c \"Asterisk User\""
else
	r_cmd "echo Asterisk User already added"
fi

# install asterisk
e_log "install asterisk11"
r_cmd "$yum -y install asterisk11 asterisk11-configs asterisk11-addons asterisk11-curl asterisk11-flite asterisk11-flite-debuginfo asterisk11-odbc"

# install sound packages
e_log "install linux sound packages"
r_cmd "$yum -y install mpg123 sox audiofile-devel libresample-devel gsm-devel #esound-devel libtool-ltdl"

# install g792
e_log "install g792 codec"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/fbcfbaf38f05b3d4f5cb55a03f00cfd22230b930/get/asterisk/g729/x86_64/codec_g729-ast110-gcc4-glibc-x86_64-pentium4.so' '/usr/lib64/asterisk/modules/codec_g729.so'"
r_cmd "chmod 0755 /usr/lib64/asterisk/modules/codec_g729.so"

# chown asterisk instalation
e_log "chown asterisk instalation"
r_cmd "chown -R asterisk. /var/run/asterisk"
r_cmd "chown -R asterisk. /var/log/asterisk"
r_cmd "chown -R asterisk. /var/spool/asterisk"
r_cmd "chown -R asterisk. /var/lib/asterisk"

# remove old kernels
e_log "remove linux old kernels"
r_cmd "$yum -y install yum-utils"
package_cleanup=$(command -v package-cleanup)
r_cmd "$package_cleanup -y --oldkernels --count=2"

# asterisk start
e_log "asterisk start"
r_cmd "$service asterisk start"

# reboot
n_reboot 3



ZNS3:
# install asterisk sounds
e_log "install asterisk sounds"
r_cmd "$yum -y install asterisk-sounds-core-en-alaw asterisk-sounds-core-en-ulaw asterisk-sounds-extra-en-alaw asterisk-sounds-extra-en-ulaw"
r_cmd "chown -R asterisk. /var/lib/asterisk"

# bash profile.d
e_log "restore normal bash: /etc/profile.d/bash.sh"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/0a5babdefc28075111239137b2b27ac09edb2b42/get/centos/6/x86_64/bash.sh' '/etc/profile.d/bash.sh'"

# remove $goto_label
e_log "delete: $goto_label"
r_cmd "rm -f $goto_label"

# asterisk -rx 'file convert /var/lib/asterisk/sounds/en/demo-echotest.gsm /var/lib/asterisk/sounds/en/demo-echotest.g729'
# core set debug 4; core set verbose 4

# secure machine: iptables ssh
