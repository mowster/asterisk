#!/bin/bash
#version: 2.4

BASH_VERSION='2.4'

TMP="$(dirname $(mktemp -u))"
LAST_TMP=$(mktemp $TMP/bash.XXXXXXXX)


function valid_ip(){
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

echo ""
echo "Welcome on `hostname`."
echo -e "Server:" `cat /etc/redhat-release`
echo -e "Kernel Details:" `uname -smr`
echo -ne "Server time: "; date
echo ""
echo -ne "Uptime: "; uptime | awk -F'( |,|:)+' '{if ($7=="min") m=$6; else {if ($7~/^day/) {d=$6;h=$8;m=$9} else {h=$6;m=$7}}} {print d+0,"days,",h+0,"hours,",m+0,"minutes"}'
echo ""

last > $LAST_TMP
last_total=$(cat $LAST_TMP | wc -l)

for i in $(seq 1 $last_total); 
do 	
	last=`cat $LAST_TMP | grep "$(id -u -n)" | awk NR==$i{'print $3","$8'}`
	
	IFS=',' read -ra array <<< "$last" 
	
	if valid_ip ${array[0]} && [ "${array[1]}" != "still" ]; then 
		ip=${array[0]}
		break
	fi	
done

if [[ ! -z "$ip" ]]; then
	time=`cat $LAST_TMP | grep "$(id -u -n)" | awk NR==$i{'print $4,$5,$6,$7" - "$9,$10'}`
	echo "Last login: $time from `host $ip | awk {'print $5'}` "
	echo ""
fi

rm -f $LAST_TMP

