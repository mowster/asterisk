#!/bin/bash
#version: 1.2

version="1.2"

file="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")" # files
folder="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
tmp_folder="$(dirname $(mktemp -u))"
sys_folder="/etc/system.d"

lockfile="/var/lock/`basename $0`"
lockfd=99

log=1 # log
log_file="$sys_folder/install/log/grub2.log"
yum_file="$sys_folder/install/log/yum.log"
log_error="$sys_folder/install/log/error.log"


e_log(){
	if [[ $log == 1 ]]; then 
		if [ ! -f $log_file ]; then mkdir -p "$(dirname "$log_file")"; touch $log_file; fi # check if log_file exists
		if [[ "$2" == "" ]]; then i=$[i + 1]; fi # keep counter if not blank
		if [[ "$i" -gt "2" ]]; then dt=$(date '+%d/%m/%Y %H:%M:%S,%3N'); else dt=$(date '+%d/%m/%Y %H:%M:%S,%3N +%Z'); fi # show timezone i>2
		
		echo -e "[ $dt ] ($i)" $1 >> $log_file
	fi
	
	echo -e ""
	if [[ "$2" != "off" ]]; then # display only selected logs
		echo -e "( $i ) $1"
	else
		echo -e "( $i ) Loading..."
	fi 
}

r_cmd(){
	
	exe=$1
	tmp_file=$(mktemp $tmp_folder/$file.XXXXXXXXXX)
	
	# execute command	
	if [[ "$exe" =~ ^$curl.* ]]; then	
		IFS=" "; declare -a Array=($*) 
		url=`echo "${Array[1]//\'}"`
		saved_file=`echo "${Array[2]//\'}"`
		
		NC=$($curl -s -w "%{size_download} %{http_code} %{speed_download} %{time_total}" -o $saved_file $url)
		set -- "$NC" 
		IFS=" "; declare -a Array=($*) 
		size_download="${Array[0]}"
		http_code="${Array[1]}"
		speed_download="${Array[2]}"
		time_total="${Array[3]}"

		if [ $size_download -eq 0 ] || [ $http_code -ne '200' ]; then STATUS=1; else STATUS=0; fi
	
		echo 'url: '$url > $tmp_file		
		printf 'size downloaded: ' >> $tmp_file; printf $size_download | awk '{ foo = $1 / 1024 ; print foo " Kb" }' >> $tmp_file
		echo 'http_code: '$http_code >> $tmp_file
		printf 'download speed: ' >> $tmp_file; printf $speed_download | awk '{ foo = $1 / 1024 ; print foo " Kb/s" }' >> $tmp_file		
		echo 'total time: '$time_total' s' >> $tmp_file
		echo 'file destination: '$saved_file >> $tmp_file
		
		exe="$curl -s -o $saved_file --compressed $url"
		printf "# $exe \n"
		cat $tmp_file
		
		
	else
		printf "# $exe "
		
		NC="$(`eval $exe > $tmp_file`)" 
		STATUS=$?
		
		if [[ "$2" != "" ]] && [ $2 -eq 0 ]; then STATUS=1; fi # log error from r_cmd 
	fi
	
	
	# report
	dt=$(date '+%H:%M:%S,%3N')
		
	cat $tmp_file | sed -r 's/\x1B\(B\x1B\[m//g' | sed -r 's/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g' > $tmp_file.tmp
		
	if [[ $log == 1 ]] && [ $STATUS -eq 0 ]; then 	
		echo -e "$dt #" $exe >> $log_file	
		
		# write in yum.log
		if [[ "$exe" =~ ^$yum.* ]]; then
			echo -e "$dt #" $exe >> $yum_file	
			$head --lines=-0 $tmp_file.tmp >> $yum_file	
		else
			$head --lines=-0 $tmp_file.tmp >> $log_file		
		fi
		
	fi	
	
	red=$(tput setaf 1); green=$(tput setaf 2); normal=$(tput sgr0)
	
	if [ $STATUS -eq 0 ] ; then 
		printf "[ "$green" OK "$normal" ]\n"
				
		rm -f $tmp_file $tmp_file.tmp
	else 
		touch $log_error
		echo -e "[ $dt ] #" $exe > $log_error			
		$head --lines=-0 $tmp_file.tmp >> $log_error
		
		printf "[ "$red" Fail "$normal" ]\n"			
		
		rm -f $tmp_file $tmp_file.tmp
		exit 1
	fi 
	
}


# _ commands
commands=( "yum" "curl" "head" "shutdown" "grub-mkconfig" "grub-install" )
for command in "${commands[@]}"
do
	eval "${command//-/_}=$(command -v $command)"
done


# _ lock file
_lock()             { flock -$1 $lockfd; }
_no_more_locking()  { _lock u; _lock xn && rm -f $lockfile; }
_prepare_locking()  { eval "exec $lockfd>\"$lockfile\""; trap _no_more_locking EXIT; }

_prepare_locking

exlock_now()        { _lock xn; }  # obtain an exclusive lock immediately or fail
exlock()            { _lock x; }   # obtain an exclusive lock
shlock()            { _lock s; }   # obtain a shared lock
unlock()            { _lock u; }   # drop a lock

exlock_now || exit 1


e_log "install required packages"
r_cmd "$yum -y install bison gcc flex make"


e_log "download: grub2"
r_cmd "$curl 'https://bitbucket.org/mowster/asterisk/raw/1ff594c5dc453d25fd55ca5d8e2bc996db842f82/get/centos/6/x86_64/grub2/grub-2.00.tar.gz' '/usr/src/grub-2.00.tar.gz'"


e_log "install: grub2"
r_cmd "tar -xzvf /usr/src/grub-2.00.tar.gz -C /usr/src"
r_cmd "cd /usr/src/grub-2.00/ && ./configure"
r_cmd "cd /usr/src/grub-2.00/ && make"
r_cmd "cd /usr/src/grub-2.00/ && make install"
r_cmd "mkdir -p /boot/grub && mkdir -p /boot/grub/fonts && mkdir -p /boot/grub/i386-pc && mkdir -p /boot/grub/locale && mkdir -p /usr/local/etc/default"

grub=
grub+=$'GRUB_TIMEOUT=1'
grub+=$'GRUB_DISTRIBUTOR="$(sed '\''s, release .*$,,g'\'' /etc/system-release)"\n'
grub+=$'GRUB_DEFAULT=saved\n'
grub+=$'GRUB_DISABLE_SUBMENU=true\n'
grub+=$'GRUB_TERMINAL="serial console"\n'
grub+=$'GRUB_DISABLE_RECOVERY=true\n'
grub+=$'GRUB_DISABLE_OS_PROBER=true\n'
grub+=$'GRUB_DISABLE_LINUX_UUID=true\n'
grub+=$'GRUB_GFXPAYLOAD_LINUX=text\n'
grub+=$'\n'
grub+=$'# Glish\n'
grub+=$'GRUB_SERIAL_COMMAND="serial --speed=19200 --unit=0 --word=8 --parity=no --stop=1"\n'
grub+=$'# Lish\n'
grub+=$'# GRUB_CMDLINE_LINUX="crashkernel=auto rhgb console=ttyS0,19200n8 net.ifnames=0"\n'


if [[ `cat /usr/local/etc/default/grub` != *`echo $grub`* ]]; then
	echo $grub >> /usr/local/etc/default/grub
else
	r_cmd "echo /usr/local/etc/default/grub already tweaked"
fi


e_log "config boot"
r_cmd "$grub_mkconfig -o /boot/grub/grub.cfg"


e_log "grub install"
r_cmd "$grub_install --grub-setup=/bin/true /dev/null"


e_log "remove: grub"
r_cmd "$yum -y remove grub"


$shutdown -r now
